const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const mongoose = require('mongoose');
const PORT = 3007;

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Mongoose connection
    //mongoose.connection(<connection string>, {options});
    //process.env = ${variable}, MONGO_URL in .env
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

//DB connection notification (test connection)
    //user connection property of mongoose
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error:')); //return issue upon error
db.once("open", () => console.log(`Connected to database`)); //

//solution1: create user schema
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `Name is required.`]
    },
    password: {
        type: String,
        required: [true, 'Password is required.']
    },
    isAdmin: {
        type: Boolean,
        required: [false, 'Admin classification is required.']
    }
})

//solution2: create User model
const userMod = mongoose.model(`userMod`, userSchema);

//solution3: Post /signup
app.post("/signup", (req, res) => {
    //if (req.body !== undefined){

        userMod.findOne({name: req.body.name}).then((result, err) => {
            console.log(result) //document
     
            //if task exists in the database, we return a message 'Duplicate task found'
            if (result != null && result.name == req.body.name){
                return res.send(`Duplicate user found.`)
            } else {    
                userMod.create(req.body);
                res.send(`User ${req.body.name} has been registered.`);
            }

        
    })
})


app.listen(PORT, () => console.log(`Server connected at port ${PORT}`));